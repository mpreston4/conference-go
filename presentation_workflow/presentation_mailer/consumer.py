import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


while True:
    try:

        def process_approval(ch, method, properties, body):
            content = json.loads(body)
            send_mail(
                "Your presentation has been accepted",
                f"{content['presenter_name']}, we're happy to tell you that your presentation, {content['title']} has been accepted",
                "example@gmail.com",
                [content["presenter_email"]],
                fail_silently=False,
            )
            print("  Received %r" % body)

        def process_rejection(ch, method, properties, body):
            content = json.loads(body)
            send_mail(
                "Your presentation has been rejected",
                f"{content['presenter_name']}, we're sorry to tell you that your presentation, {content['title']} has been rejected",
                "example@gmail.com",
                [content["presenter_email"]],
                fail_silently=False,
            )
            print("  Received %r" % body)

        parameters = pika.ConnectionParameters(host="rabbitmq")
        # refers to rebabbit container
        connection = pika.BlockingConnection(parameters)
        # make a connection with rabbitmq container
        channel = connection.channel()
        # setting up a channel within the connection

        channel.queue_declare(queue="presentation_approvals")
        channel.basic_consume(
            queue="presentation_approvals",
            on_message_callback=process_approval,
            auto_ack=True,
        )

        channel.queue_declare(queue="presentation_rejections")
        channel.basic_consume(
            queue="presentation_rejections",
            on_message_callback=process_rejection,
            auto_ack=True,
        )

        # consumes any "message" regardless of wheather it's a rejection or acception
        channel.start_consuming()

    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
