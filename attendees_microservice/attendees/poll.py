import json
import requests

from .models import ConferenceVO


def get_conferences():
    url = "http://monolith:8000/api/conferences/"
    response = requests.get(url)
    # extract the data from the content dictionary as a result of
    # request.get(url). Response is the list of conferences
    # that would be returned from requests.get(url).
    # Content is a default parameter we can pass as a method to response.
    content = json.loads(response.content)
    # For each conference in the list of conferences
    for conference in content["conferences"]:
        ConferenceVO.objects.update_or_create(
            # If a ConferenceVO object exists with the following parameter
            # "import_href" = conference["href"], then
            # update with "name"=conference["name"].
            import_href=conference["href"],
            # update the ConferenceVO with {"name":conference["name"]}
            defaults={"name": conference["name"]},
        )
        # the end result is a ConferenceVO objecrt with has import_href
        # and a name:conference["name"] dictionary
