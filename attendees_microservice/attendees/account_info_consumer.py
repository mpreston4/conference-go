from datetime import datetime
import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
import requests
from django.core.exceptions import ObjectDoesNotExist

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "attendees_bc.settings")
django.setup()

from attendees.models import AccountVO


def update_accountVO(ch, method, properties, body):
    content = json.loads(body)
    first_name = content["first_name"]
    last_name = content["last_name"]
    email = content["email"]
    is_active = content["is_active"]
    updated_string = content["updated"]
    updated = datetime.fromisoformat(updated_string)
    try:
        if is_active:
            AccountVO.objects.update_or_create(
                email=email,
                defaults={
                    "first_name": first_name,
                    "last_name": last_name,
                    "is_active": is_active,
                    "updated": updated,
                },
            )
        else:
            AccountVO.objects.filter(email=email).delete()
    except ObjectDoesNotExist:
        print(f"Sorry, we cannot find an account associated with {email}")


while True:
    try:
        parameters = pika.ConnectionParameters(host="rabbitmq")
        # refers to rabbit container
        connection = pika.BlockingConnection(parameters)
        # make a connection with rabbitmq container
        channel = connection.channel()
        # setting up a channel within the connection
        channel.exchange_declare(
            exchange="account_info", exchange_type="fanout"
        )

        # rabbit will create an automatic queue name, and we are storing this name
        result = channel.queue_declare(queue="", exclusive=True)
        queue_name = result.method.queue

        channel.queue_bind(
            exchange="account_info",
            queue=queue_name,
        )

        channel.basic_consume(
            queue=queue_name,
            on_message_callback=update_accountVO,
            auto_ack=True,
        )

        # consumes any "message" regardless of wheather it's a rejection or acception
        channel.start_consuming()

    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
