from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .acls import get_photo, get_weather_data

from .models import Conference, Location, State
from common.json import ModelEncoder


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences}, encoder=ConferenceListEncoder
        )
        # why is each url "api/conferences/id/"?
        # how does json.py know to loop through to get all
        # url links?
    else:
        content = json.loads(request.body)
        # someone has entered data via. json and json.loads has
        # converted to a python dictionary
        try:
            # now we have to see if a "location" key/value pair
            # was entered b/c if it was, since it's a ForeignKey
            # it has to exist.
            location = Location.objects.get(id=content["location"])
            # this gets the location object specified by the location
            # that the user enters. if they specify "5", then it gets
            # the location object with id = 5.
            content["location"] = location
            # this updates the location dictionary as
            # key = "location" & value = location object.
            # Note, only location object parameters that we've
            # specified in LocationListEncoder will show up (name & id)
        except Location.DoesNotExist:
            return JsonResponse({"message": "Invalid location id"}, status=400)
        conference = Conference.objects.create(**content)
        # after testing if everything was entered correctly, create
        # a new conference object (instance)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            # safe=false removes stipulation that JSON Response
            # has to be in dictionary format
            safe=False,
        )

    # conferences = Conference.objects.all()
    # return JsonResponse(
    #     {"conferences": conferences}, encoder=ConferenceListEncoder
    # )


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name", "id"]


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_conference(request, id):
    if request.method == "GET":
        conference = Conference.objects.get(id=id)
        # this conference is an object (it's an instance of the Conference class)
        weather = get_weather_data(
            conference.location.city,
            # this gets the conference object==> location object ==> city variable
            conference.location.state
            # this gets the conference object==> location object ==> state variable
        )

        return JsonResponse(
            {"conference": conference, "weather": weather},
            # we want json to return 2 dictionaries (conference & weather)
            # we don't want to store weather data in the coference object b/c
            # we don't want to change anything in the actual database.
            encoder=ConferenceDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "location" in content:
                location = Location.objects.get(id=content["location"])
                content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location"},
                status=400,
            )

        Conference.objects.filter(id=id).update(**content)
        conference = Conference.objects.get(id=id)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )

    # conference = Conference.objects.get(id=id)
    # return JsonResponse(
    #     conference, encoder=ConferenceDetailEncoder, safe=False
    # )


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse(
            {"locations": locations}, encoder=LocationListEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"}, status=400
            )

        content["image_url"] = get_photo(content["city"])

        location = Location.objects.create(**content)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )

    # response = []
    # locations = Location.objects.all()
    # for location in locations:
    #     response.append(
    #         {
    #             "name": location.name,
    #             "href": location.get_api_url(),
    #         }
    #     )

    # return JsonResponse({"locations": response})


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "image_url",
    ]

    # THIS OVERRIDES THE 'get_extra_data' function from the parent class
    # which is the Model Encoder class. We are returning a dictionary
    # with "state"/abbreviation as the key/value pairs
    def get_extra_data(self, o):
        return {"state": o.state.abbreviation}


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, id):
    """
    Returns the details for the Location model specified
    by the id parameter.

    This should return a dictionary with the name, city,
    room count, created, updated, and state abbreviation."""
    if request.method == "GET":
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "state" in content:
                state = State.objects.get(name=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )

        Location.objects.filter(id=id).update(**content)
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
