from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city):
    url = f"https://api.pexels.com/v1/search?query={city}"
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, headers=headers)
    # the get method for requests allows us to define the url, and
    # headers parameter. Adding headers=headers defining the
    # headers parameter with the variable we have stored above.

    content = response.json()
    try:
        return content["photos"][0]["src"]["original"]
        # if the key doesn't exist it will throw an error
    except (KeyError, IndexError):
        return None


def get_weather_data(city, state):
    geo_params = {
        # this is the first api we request
        "q": f"{city},{state},US",
        # q is required, read the weather api docs.
        # we must define city, state, country
        "limit": 1,
        # we are limiting our data to 1. we don't need multiple data
        # for the city
        "appid": OPEN_WEATHER_API_KEY,
        # this is our API KEY for security
    }
    geo_coding_url = "http://api.openweathermap.org/geo/1.0/direct"
    # since we are going to define parameters later, we will
    # only include the url just before the "?"
    response = requests.get(geo_coding_url, params=geo_params)
    # go get the weather data. arguments being the url
    # and the params as we've defined, this will be stored
    # as a python dictionary
    # content = json.loads(response)
    # convert from python dictionary to json string

    content = response.json()
    try:
        latitude = content[0]["lat"]
        # gets the first key
        longitude = content[0]["lon"]
    except (KeyError, IndentationError):
        return None

    weather_url = "https://api.openweathermap.org/data/2.5/weather"
    # url from the docs. this url makes a GET request
    weather_params = {
        "lat": latitude,  # passing in lattitude (from the request above)
        "lon": longitude,  # we're passing in longitdue
        "appid": OPEN_WEATHER_API_KEY,  # we're using our API key (security)
        "units": "imperial",  # we're choosing to define units
    }

    response = requests.get(weather_url, params=weather_params)
    content = response.json()

    try:
        description = content["weather"][0]["description"]
        temp = content["main"]["temp"]
    except (KeyError, IndentationError):
        return None

    return {"description": description, "temp": temp}
