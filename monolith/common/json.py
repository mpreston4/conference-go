from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}

    def default(self, o):
        if isinstance(o, self.model):
            d = {}
            if hasattr(o, "get_api_url"):
                # if object has "get_api_url" parameter, then do something
                d["href"] = o.get_api_url()
                # append dictionary with key as "href" and value as url link
            for property in self.properties:
                value = getattr(o, property)
                if property in self.encoders:
                    # if property is in the encoders dictionary
                    encoder = self.encoders[property]
                    # get the encoders key and save to variable
                    value = encoder.default(value)  # WHAT DOES THIS LINE DO?
                d[property] = value
            d.update(self.get_extra_data(o))  # what is this doing?
            return d
        else:
            return super().default(o)

    def get_extra_data(self, o):
        return {}


# # Create a ModelEncoder class that will inherit from JSONEncoder,
# # QuerySetEncoder, and DateEncoder
# class ModelEncoder(JSONEncoder, QuerySetEncoder, DateEncoder):
#     # create an open encoders dictionary
#     encoders = {}

#     # Define a function that will take self, and the object
#     # as the arguments
#     def default(self, o):
#         # test to see if the object (o) is
#         # an instance of the model
#         if isinstance(o, self.model):
#             # create an empty dictionary
#             d = {}
#             # test to see if object instance from class has an api url
#             if hasattr(o, "get_api_url"):
#                 # append key/value as "href"/url
#                 d["href"] = o.get_api_url()
#             # loop through properties in properties list
#             for property in self.properties:
#                 # get "property" attribute from object instance
#                 value = getattr(o, property)
#                 # test to see if property is in encoders dictionary
#                 if property in self.encoders:
#                     # get the encoder key and save as "encoder"
#                     encoder = self.encoders[property]
#                     #
#                     value = encoder.default(value)  # WHAT DOES THIS LINE DO?
#                 # update dictionary with key/value as property/value
#                 d[property] = value
#             # update dictionary with any extra data needed
#             d.update(self.get_extra_data(o))
#             # return the dictionary
#             return d
#         else:
#             return super().default(o)

#     def get_extra_data(self, o):
#         return {}
