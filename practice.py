# from django.http import JsonResponse
# from django.views.decorators.http import require_http_methods
# import json
# from events.acls import get_photo, get_weather_data

# from events.models import Conference, Location, State
# from common.json import (
#     ModelEncoder,
# )  # require http decorator for function to run


# class LocationListEncoder(ModelEncoder):
#     model = Location
#     properties = ["name", "id"]


# class ConferenceDetailEncoder(ModelEncoder):
#     model = Conference
#     properties = [
#         "name",
#         "description",
#         "max_presentations",
#         "max_attendees",
#         "starts",
#         "ends",
#         "created",
#         "updated",
#         "location",
#     ]
#     encoders = {
#         "location": LocationListEncoder(),
#     }


# class ConferenceListEncoder(ModelEncoder):
#     model = Conference
#     properties = ["name"]


# # add require http method decorator
# # this specifies which http methods will invoke this funciton
# @require_http_methods(["GET", "POST"])
# # define the funciton (list conferences in this case)
# def api_list_conferences(request):
#     # if the request method is a GET, do somthing
#     if request.method == "GET":
#         # get all of the conference object instances
#         conferences = Conference.objects.all()
#         # return the list of conferences via JSON response
#         # need to elaborate here. What does encoders do?
#         return JsonResponse(
#             {"conferences": conferences}, encoders=ConferenceListEncoder
#         )
#     # if the request method is a "POST", do something
#     else:
#         # convert the "body" from python dictionary
#         # to json dictionary
#         content = json.loads(request.body)
#         try:
#             # Think of special cases, like Foregin Keys.
#             # we need to see if one of the inputs
#             # in the content dictionary (human response) was a
#             # ForeginKey, that exists.

#             # This is finding the location, and setting it equal to
#             # the id parameter. If we try to set id = a number
#             # that doesn't exist, it will throw us an error.
#             location = Location.objects.get(id=content["location "])

#             # Now that we have the location instance of that given
#             # id #, we want to update the content dictionary with
#             # that the key/value pair ("location": id #)
#             content["location"] = location
#         # here we will add an error type to the except block
#         except Location.DoesNotExist:
#             return JsonResponse({"message": "Invalid locaiton id"}, status=400)
#         # now we will create a conferene object
#         conference = Conference.objects.create(**content)
#         # now we will return the conference object
#         return JsonResponse(
#             conference, encoder=ConferenceDetailEncoder, safe=False
#         )


# # we want to require a request method & type
# @require_http_methods(["DELETE", "GET", "PUT"])
# # define the show_conference function
# def api_show_conference(request, id):
#     if request.method == "GET":
#         conference = Conference.objects.get(id=id)
#         # we've added in live weather data.
#         # we will store the weather data of the locaiton requested
#         # this referrs to the get_weather_data funciton
#         # defined in the acls file
#         weather = get_weather_data(
#             conference.location.state, conference.location.city
#         )

#         return JsonResponse(
#             {"conference": conference, "weather": weather},
#             encoder=ConferenceDetailEncoder,
#             set=False,
#         )
#     elif request.method == "DELETE":
#         count, _ = Conference.objects.filter(id=id).delete()
#         return JsonResponse({"deleted": count > 0})
#     else:
#         # we declaring that what the body of the request held
#         # is now going to be stored in the content variable
#         # as a dictionary
#         content = json.loads(request.body)
#         # we will do a similar try/except as the "POST" request
#         # to test and see if any foregin key issues pose a threat
#         # to an error
#         try:
#             if "locaiton" in content:
#                 # save the location object associated with id= whatever
#                 # the user input
#                 location = Location.objects.get(id=content["location"])
#                 # update the content with key/value pair
#                 # "location"/location object
#                 content["location"] = location
#         except Location.DoesNotExist:
#             return JsonResponse(
#                 {"message": "Invalid location"},
#                 status=400,
#             )
#         # update the location object associated with the id
#         # with the content that was input by user
#         Conference.objects.filter(id=id).update(**content)
#         # save the Confdrence object to a variable "conference"
#         conference = Conference.objects.get(id=id)
#         return JsonResponse(
#             conference, encoder=ConferenceDetailEncoder, safe=False
#         )
